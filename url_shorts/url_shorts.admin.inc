<?php

/**
 * @file
 * URL Shorts GUI interface functions
 */

/**
 * Sets up the URL shortener form
 * 
 * @param type form
 * @param type form_state
 * 
 * return form
 */
function url_shorts_edit_form($form, &$form_state, $type) {
  $form['to_url'] = array(
    '#type' => 'textfield',
    '#title' => t('To:'),
    '#description' => t('Enter a URL to shorten'),
    '#maxlength' => 255
  );
  $form['from_url'] = array(
    '#type' => 'textfield',
    '#title' => t('From:'),
    '#field_prefix' => $GLOBALS['base_url'] . '/' . (variable_get('clean_url', 0) ? '' : '?q='),
    '#description' => t('You may enter a custom URL here, or click Generate Random for a unique URL'),
    '#maxlength' => 255
  );
  $form['generate_random'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Random'),
    '#ajax' => array(
      'callback' => 'url_shorts_ajax_generate_url'
    )
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/search/url_shorts'
  );

  return $form;
}

// end url_shorts_edit_form
